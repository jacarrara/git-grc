## GIT-GRC

Docker image with git and git codecommit helper

# Usage

```

docker run -t jacarrara/git-grc [options]

```

# License

MIT
