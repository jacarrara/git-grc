FROM python:3.9-alpine
RUN apk add git
RUN pip install git-remote-codecommit
ENTRYPOINT ["/usr/bin/git"]
CMD []
